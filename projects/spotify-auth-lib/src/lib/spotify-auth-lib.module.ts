import { NgModule } from '@angular/core';
import { SpotifyAuthLibComponent } from './spotify-auth-lib.component';

@NgModule({
  declarations: [SpotifyAuthLibComponent],
  imports: [
  ],
  exports: [SpotifyAuthLibComponent]
})
export class SpotifyAuthLibModule { }
