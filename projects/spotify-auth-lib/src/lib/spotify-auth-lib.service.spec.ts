import { TestBed } from '@angular/core/testing';

import { SpotifyAuthLibService } from './spotify-auth-lib.service';

describe('SpotifyAuthLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpotifyAuthLibService = TestBed.get(SpotifyAuthLibService);
    expect(service).toBeTruthy();
  });
});
