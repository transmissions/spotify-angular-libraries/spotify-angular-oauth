import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotifyAuthLibComponent } from './spotify-auth-lib.component';

describe('SpotifyAuthLibComponent', () => {
  let component: SpotifyAuthLibComponent;
  let fixture: ComponentFixture<SpotifyAuthLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotifyAuthLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotifyAuthLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
