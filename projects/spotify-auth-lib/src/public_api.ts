/*
 * Public API Surface of spotify-auth-lib
 */

export * from './lib/spotify-auth-lib.service';
export * from './lib/spotify-auth-lib.component';
export * from './lib/spotify-auth-lib.module';
